module.exports = app => {
  const studenti = require("../controller/studenti.controller.js");
  const multer = require("multer");
  const path = require("path");

  const upload = multer({
    dest: path.join(__dirname, "../images/")
    // you might also want to set some limits: https://github.com/expressjs/multer#limits
  });

  // Default route
  app.get("/", (req, res) => {
    res.send();
  });

  // Create a new Studente
  app.post("/studenti", studenti.create);

  // Retrieve all Studente 
  app.get("/studenti", studenti.findAll);

  // Retrieve a single Studente with Id
  app.get("/studenti/:studenteId", studenti.findOne);

  // Update a Studente  with Id
  app.put("/studenti/:studenteId", studenti.update);
  
  // Delete a Studente with Id
  app.delete("/studenti/:studenteId", studenti.delete);

  // Cancella tutti gli studenti
  app.delete("/studenti", studenti.deleteAll);

  // Ottieni tutte le sezioni possibili
  app.get("/sezioni", studenti.getSezioni);

  // Ottieni tutte le specializzazioni possibili
  app.get("/specializzazioni", studenti.getSpecializzazioni);

  // Ottieni tutti gli anni possibili
  app.get("/anni", studenti.getAnni);

  // Controlla se un'immagine di uno specifico studente esiste
  app.get("/immagini/check/:studenteId", studenti.checkImmagine);

  // Ottieni l'immagine di uno specifico studente
  app.get("/immagini/:studenteId", studenti.getImmagine);

  app.post("/upload/:studenteId",upload.single("file"),studenti.uploadImage);

  app.get("/maxID", studenti.getMaxID);
};