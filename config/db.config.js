module.exports = {
  HOST: "localhost",
  USER: "root",
  PASSWORD: "root",
  DB: "DB_alunni",
  // Impostare a false se non si vuole l'esecuzione del file sql in automatico all'avvio del server
  autoRunSql: true
};