const url = "http://localhost:8080";

const app = new Vue({
    el: "#app",
    data: {      
        students: [], // Array contenente tutti gli studenti
        // currentEditStudent: NaN, // Non piú utilizzata

        // Parametri per la gestione degli input
        setting : {
            // Ottenuti a startup 
            sezioni: [],  // Lista sezioni
            specializzazioni: [], // Lista specializzazioni
            anni: [], // Lista anni
            // Costante, link API foto profilo utenti
            linkProfileImage: "https://eu.ui-avatars.com/api/?background=random&name=" // + nome + cognome        
        },

        currentSort:'Nome', // Nome del parametro su cui viene fatto l'ordinamento
        currentSortDir:'asc', // Ordinamento di tipo ascendente o discendente
        pageSize:10, // Costante, dimensione pagine
        currentPage:1, // Pagina corrente da visualizzare
        searchInfo: {
            canSearch: true, // Costante, da impostare su false per debug
            typeOfSearch: "Nome", // Nome del parametro su cui viene fatta la ricerca
            searchVal: ""
        }
    },
    // I metodi nel mounted vengono avviati al caricamento della pagina
    mounted() {
       this.loadStudents(false); // Inizializza studenti
       this.loadSettings(); // Inizializza setting
    },
    methods: {
        // Carica nuovo studente
        uploadStudent(student){          
            // Ottieni massimo id in modo da sapere in anticipo che id avrá il nuovo studente aggiunto
            fetch(url + "/maxID", {
                method: "GET"
            })
            .then(response => response.json())
            .then((data) => {    
                //Carica immagine legata all'id che si é ottenuto prima + 1 
                var formData = new FormData();
                formData.append("file", student.Image);
                fetch(url + `/upload/${++data.id}`, {
                    method: "POST",
                    body: formData
                }).then(()=>{})
            }).then(response => 
                //Carica nuovo studente con tutte le informazioni
                fetch(url + "/studenti", {
                    method: "POST",
                    body: JSON.stringify(student),
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then(() => {
                    this.currentPage = 1;
                    this.loadStudents(true)                                                             
                }).then()
            )
        },
        // Scarica studenti
        loadStudents(reload){
            fetch(url + "/studenti", {
                method: "GET"
            })
            .then(response => response.json())
            .then((data) => {                
                this.students = data;      
                if(reload == true){
                    console.log('ordinamento per id')
                    this.currentSortDir = "desc";
                    this.sort('ID'); 
                }                                       
            })
        },
        // Elimina studente
        deleteStudent(student){
            fetch(url + "/studenti/" + student.ID, {
                method: "DELETE"
            })
            .then(
                this.loadStudents(false)
            )
        },
        // Aggiorna studente
        updateStudent(student){
            //console.log("Updading")
            fetch(url + "/studenti/" + student.ID, {
                method: "PUT",
                body: JSON.stringify(student),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then(
                this.loadStudents(false)                                                       
            )
            .then(               
                this.editStudent = NaN 
            )          
        },
        // Elimina tutti gli studenti
        deleteAllStudents(){
            fetch(url + "/studenti/", {
                method: "DELETE"
            })
            .then(
                this.loadStudents(false)
            )
        },      
        // Algoritmo di ordinamento
        sort:function(s) {
            this.currentPage = 1;
            //if s == current sort, reverse
            if(s === this.currentSort) {
            this.currentSortDir = this.currentSortDir === 'asc'?'desc':'asc';
                }
            this.currentSort = s;
            this.students.sort((a,b) => {
                let modifier = 1;
                if(this.currentSortDir === 'desc') modifier = -1;
                if(a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
                if(a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
                return 0;
            })
        },
        nextPage:function() {
            if((this.currentPage*this.pageSize) < this.students.length && (this.sortedStudents.length == this.pageSize)) this.currentPage++;
        },
        prevPage:function() {
            console.log("lmao")
            if(this.currentPage > 1) this.currentPage--;
        },
        loadSettings: function() {          
            // Scaricaze lista sezioni
            fetch(url + "/sezioni", {method: "GET"})
            .then(response => response.json())
            .then((data) => {this.setting.sezioni = data})
            // Scaricaze lista specializzazioni
            fetch(url + "/specializzazioni", {method: "GET"})
            .then(response => response.json())
            .then((data) => {
                this.setting.specializzazioni = data})
            // Scaricaze lista anni
            fetch(url + "/anni", {method: "GET"})
            .then(response => response.json())
            .then((data) => {this.setting.anni = data})
        },
    }, 
    computed: {
        sortedStudents: function() {
            return this.students.filter((student) => {
                if(this.searchInfo.canSearch == true){
                    if(student[this.searchInfo.typeOfSearch].includes(this.searchInfo.searchVal.toUpperCase())){
                        return true;
                    } else {
                        return false;
                    }
                } else {
                     return true;
                }               
            }).filter((row, index) => {
                let start = (this.currentPage-1)*this.pageSize;
                let end = this.currentPage*this.pageSize;
                if(index >= start && index < end) return true;
            });
        },      
    }, 
    template: `
        <div>           
            <div class="table-container">
                <add-user-component 
                    v-bind:setting="setting"
                    @uploadStudent="uploadStudent"                  
                    @deleteAll="deleteAllStudents">
                </add-user-component>
                <search-bar-component
                    v-bind:searchInfo="searchInfo"
                    v-bind:currentPage="currentPage"
                    @nextPage="nextPage"
                    @prevPage="prevPage">                   
                </search-bar-component>
                <div class="row first-row align-items-center">
                    <div class="col-md-ID"> 
                        # 
                    </div>
                    <div class="col-md-Img">
                    </div>
                    <div class="col-md-Nome" v-on:click="sort('Nome')"> 
                        <p>Nome</p> 
                    </div>
                    <div class="col-md-Cognome" v-on:click="sort('Cognome')"> 
                        <p>Cognome</p> 
                    </div>
                    <div class="col-md-Residenza" v-on:click="sort('Residenza')"> 
                        <p>Residenza</p> 
                    </div>
                    <div class="col-md-DataNascita" v-on:click="sort('Nome')"> 
                        <p>DataNascita</p> 
                    </div>
                    <div class="col-md-Anno" v-on:click="sort('Anno')"> 
                        <p>Anno</p> </div>
                    <div class="col-md-Sezione" v-on:click="sort('Sezione')"> 
                        <p>Sez.</p> 
                    </div>
                    <div class="col-md-Specializzazione" v-on:click="sort('Specializzazione')"> 
                        <p>Spec.</p> 
                    </div>  
                    <div class="col-md-Azione">
                                              
                    </div>              
                </div>
                
                <row-component 
                    v-for="(student, index) in sortedStudents" 
                    v-bind:student="student" 
                    v-bind:setting="setting"
                    v-bind:index="index"
                    v-bind:pageSize="pageSize"
                    v-bind:currentPage="currentPage"
                    v-bind:key="student.ID"
                    @removeStudent="deleteStudent"
                    @updateStudent="updateStudent">
                </row-component>
            </div>
        </div>
    `
})