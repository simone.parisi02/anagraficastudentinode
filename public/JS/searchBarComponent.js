Vue.component('search-bar-component', {
    props: ['searchInfo', 'currentPage'],
    computed: {   
    },
    methods: {
        prevPage: function() {this.$emit('prevPage');},
        nextPage: function() {this.$emit('nextPage');}
    },
    template: `
        <div class="row align-items-center">
            <div class="col-md-AttivaDisattiva">               
                Cerca per:
            </div>
            <div class="col-md-opzioniRicerca">                                          
                <select class="form-control type-of-search" id="exampleFormControlSelect1"  v-model="searchInfo.typeOfSearch">
                    <option>Nome</option>
                    <option>Cognome</option>
                    <option>Residenza</option>
                    <option>DataNascita</option>
                    <option>Anno</option>
                    <option>Sezione</option>
                    <option>Specializzazione</option>
                </select>                                           
            </div>  
            <div class="col-md-barraRicerca" >    
                <input type="text" class="form-control" placeholder="Cerca"  v-model="searchInfo.searchVal">
            </div> 
            <div class="col-infoPagina">    
                Pagina:
            </div>      
            <div class="col-numPagina">
                <button type="button" class="btn btn-outline-dark" v-on:click="prevPage">Prev</button>                          
                <input type="number" :disabled="true" class="page-counter-input form-control" v-model="currentPage"> 
                <button type="button" class="btn btn-outline-dark" v-on:click="nextPage">Next</button>
            </div>
        </div>
    `
})