Vue.component('add-user-component', {
    props: ['setting']
    ,
    data() {
        return {
            newStudent: {
                Specializzazione: null,
                Anno: null,
                Sezione: null,
                Nome: null,
                Cognome: null,
                Residenza: null,
                DataNascita: null,     
                Image: null         
            },           
        }
    },
    computed: {
        allDataOk: function() {
            return (this.newStudent.Specializzazione != null &&
                this.newStudent.Anno != null &&
                this.newStudent.Sezione != null &&
                this.newStudent.Nome != null &&
                this.newStudent.Cognome != null &&
                this.newStudent.Residenza != null &&
                this.newStudent.DataNascita != null)           
        },    
    },
    methods: {
        uploadStudent: function(){
            this.$emit('uploadStudent', this.newStudent);
            this.newStudent = {
                Specializzazione: null,
                Anno: null,
                Sezione: null,
                Nome: null,
                Cognome: null,
                Residenza: null,
                DataNascita: null,
                Image: null             
            }
        },
        deleteAll: function() {
            this.$emit('deleteAll');
        },
        onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;
            this.createImage(files[0]);
        },
        createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;
        
            reader.onload = (e) => {
                vm.newStudent.Image = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        removeImage: function (e) {
            this.newStudent.Image = null;
        }
    },
    template: `
        <div class="row align-items-center">
            <div class="col-md-ID center-text-vert">               
                <p></p>                         
            </div>
            <div v-if="newStudent.Image == null" class="col-md-Img">               
                <input type="file" @change="onFileChange" class="form-control">
            </div>
            <div v-else class="col-md-Img">
                <img class="img-user" :src="newStudent.Image" @click="removeImage"/>
            </div>         
            <div class="col-md-Nome">                   
                <input  type="text" class="form-control" placeholder="Nome" v-model="newStudent.Nome">        
            </div>
            <div class="col-md-Cognome">                 
                <input type="text" class="form-control" placeholder="Cognome" v-model="newStudent.Cognome">           
            </div>
            <div class="col-md-Residenza">                 
                <input type="text" class="form-control" placeholder="Residenza" v-model="newStudent.Residenza"> 
            </div>
            <div class="col-md-DataNascita">                      
                <input type="date" class="form-control" value="newStudent.DataNascita" v-model="newStudent.DataNascita"> 
            </div>
            <div class="col-md-Anno">                       
                <select class="form-control" v-model="newStudent.Anno">
                    <option v-for="anno in setting.anni">{{anno.Anno}}</option>
                </select>       
            </div>
            <div class="col-md-Sezione">                            
                <select class="form-control" v-model="newStudent.Sezione">
                    <option v-for="sez in setting.sezioni">{{sez.Sezione}}</option>
                </select>              
            </div>
            <div class="col-md-Specializzazione">         
                <select class="form-control" v-model="newStudent.Specializzazione">
                    <option v-for="spec in setting.specializzazioni">{{spec.Specializzazione}}</option>
                </select>       
            </div> 
            <div class="col-md"> 
                <button :disabled="allDataOk == false" type="button" class="btn btn-outline-primary" v-on:click="uploadStudent()">
                    Aggiungi
                </button>                 
            </div>
        </div>
    `
})