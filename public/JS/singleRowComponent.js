
const lol = '<img v-if="editable == false" class="img-user" v-bind:src="setting.linkProfileImage + student.Nome + student.Cognome"></img>'

Vue.component('row-component', {
    props: [
        'student',
        'setting',
        'index',
        'currentPage',
        'pageSize'        
    ],
    data: function () {
        return {
          editable: false,
          tmpStudent: {},
          image: null,
          tmpImage: null
        }
    },
    methods: {
        removeStudent () {
            this.$emit('removeStudent', this.student)
        },
        updateStudent () {
            this.editable = false;
            var formData = new FormData();
            formData.append("file", this.tmpImage);
            fetch(url + `/upload/${this.student.ID}`, {
                method: "POST",
                body: formData
            }).then(data => {
                this.getImage();
                this.$emit('updateStudent', this.student)
            })        
            
        },
        enableEditing() {
            this.tmpStudent = JSON.parse(JSON.stringify(this.student));
            this.editable = true;
        },
        disableEditing () {
            this.editable = false;
            this.student = JSON.parse(JSON.stringify(this.tmpStudent));
        },      
        getImage() {
            fetch("/immagini/check/" + this.student.ID, {method: "GET"})
            .then(response => response.json())
            .then(data => {                
                if (data.link) {
                    this.image = ("/immagini/" + this.student.ID)
                } else {
                    this.image = (this.setting.linkProfileImage + this.student.Nome + this.student.Cognome)        
                }                                                     
            })
        },     
        onFileChange(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;
            this.createImage(files[0]);
        },
        createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;
        
            reader.onload = (e) => {
                vm.tmpImage = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        removeImage: function (e) {
            this.tmpImage = null;
        }        
    },
    created: function () {                   
        this.getImage();                
    },

    template: `
        <div class="row align-items-center data-row">
            <div class="col-md-ID center-text-vert">               
                <p>{{(index + 1) + ((currentPage - 1) * pageSize)}}</p>                         
            </div>
            <div class="col-md-Img">             
                <img v-if="editable == false" class="img-user" v-bind:src="image">
                <div v-else>
                    <div v-if="tmpImage == null" class="col-md-Img">               
                        <input type="file" @change="onFileChange" class="form-control">
                    </div>
                    <div v-else class="col-md-Img">
                        <img class="img-user" :src="tmpImage" @click="removeImage"/>
                    </div>  
                </div>                              
            </div>         
            <div class="col-md-Nome" v-bind:class="{'center-text-vert': !editable}">    
                <p v-if="editable == false">{{student.Nome}}</p>  
                <input v-else type="text" class="form-control" placeholder="Nome" v-model="student.Nome">        
            </div>
            <div class="col-md-Cognome" v-bind:class="{'center-text-vert': !editable}">  
                <p v-if="editable == false">{{student.Cognome}}</p>
                <input v-else type="text" class="form-control" placeholder="Cognome" v-model="student.Cognome">           
            </div>
            <div class="col-md-Residenza" v-bind:class="{'center-text-vert': !editable}">      
                <p v-if="editable == false">{{student.Residenza}}</p>      
                <input v-else type="text" class="form-control" placeholder="Residenza" v-model="student.Residenza"> 
            </div>
            <div class="col-md-DataNascita" v-bind:class="{'center-text-vert': !editable}">        
                <p v-if="editable == false">{{student.DataNascita}}</p>  
                <input v-else type="date" class="form-control" value="student.DataNascita" v-model="student.DataNascita"> 
            </div>
            <div class="col-md-Anno" v-bind:class="{'center-text-vert': !editable}">           
                <p v-if="editable == false">{{student.Anno}}</p>   
                <select v-else class="form-control" v-model="student.Anno">
                    <option v-for="anno in setting.anni">{{anno.Anno}}</option>
                </select>       
            </div>
            <div class="col-md-Sezione" v-bind:class="{'center-text-vert': !editable}">              
                <p v-if="editable == false">{{student.Sezione}}</p>  
                <select v-else class="form-control" v-model="student.Sezione">
                    <option v-for="sez in setting.sezioni">{{sez.Sezione}}</option>
                </select>              
            </div>
            <div class="col-md-Specializzazione" v-bind:class="{'center-text-vert': !editable}">    
                <p v-if="editable == false">{{student.Specializzazione}}</p>   
                <select v-else class="form-control" v-model="student.Specializzazione">
                    <option v-for="spec in setting.specializzazioni">{{spec.Specializzazione}}</option>
                </select>       
            </div>  
            <div class="col-md"> 
                <button v-if="editable == false" type="button" class="btn btn-outline-dark" v-on:click="enableEditing()">
                    Modifica
                </button>
                <button v-else class="btn btn-outline-success" v-on:click="updateStudent()">
                    Salva
                </button>
                <button v-if="editable == false" type="button" class="btn btn-outline-danger" v-on:click="removeStudent()">
                    Elimina
                </button>
                <button v-else class="btn btn-outline-danger" v-on:click="disableEditing()">
                    Annulla
                </button>
            </div>  
        </div>
    `
})