const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const app = express();
const fs = require('fs');

// parse requests of content-type: application/json
/* app.use(bodyParser.json()); */
app.use(bodyParser.json({limit: '100mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}))
app.use(cors());
app.use('/', express.static(__dirname + '/public'));

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));



//Load routes
require("./routes/routes.js")(app);

// set port, listen for requests
app.listen(8080, () => {
  console.log("Server is running");
  console.log("http://localhost:8080/");
});