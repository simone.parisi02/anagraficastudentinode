const Studente = require("../models/studente.model.js");
const path = require("path");
const fs = require('fs');
//---------------------------------------------------------------------


// Create and Save a new Studente
exports.create = (req, res) => {
  //console.log(req.body)
  Studente.create(req.body, (err, data) => {
    
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating Studenti."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------


// Retrieve all Studenti from the database.
exports.findAll = (req, res) => {
   Studente.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Studenti."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------


// Find a single Studente with a customerId
exports.findOne = (req, res) => {
   Studente.findById(req.params.studenteId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Studente with id ${req.params.studenteId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Studente with id " + req.params.studenteId
        });
      }
    } else res.send(data);
  });
};


//---------------------------------------------------------------------


// Update a Studente identified by the customerId in the request
exports.update = (req, res) => {
  Studente.update(req.body, req.params.studenteId, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while updating Studenti."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------


// Delete a Studente with the specified customerId in the request
exports.delete = (req, res) => {
    Studente.deleteById(req.params.studenteId, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Studente with id ${req.params.studenteId}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Studente with id " + req.params.studenteId
          });
        }
      } else res.send(data);
    })
};


//---------------------------------------------------------------------


// Delete all Studente from the database.
exports.deleteAll = (req, res) => {
  Studente.deleteAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while deleting Studenti."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------


exports.getSezioni = (req, res) => {
  Studente.getSezioni((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while getting Sezioni."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------



exports.getSpecializzazioni = (req, res) => {
  Studente.getSpecializzazioni((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while getting Specializzazioni."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------


exports.getAnni = (req, res) => {
  Studente.getAnni((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while getting Anni."
      });
    else res.send(data);
  });
};


//---------------------------------------------------------------------


exports.checkImmagine = (req, res) => {  
  const imagePath = path.join(__dirname, `../images/${req.params.studenteId}.jpg`)
  if(fs.existsSync(imagePath)){
    let jsonResponse = JSON.parse(`{"link":true}`)     
    res.send(jsonResponse);
  } else {
    let jsonResponse = JSON.parse(`{"link":false}`)     
    res.send(jsonResponse);
  } 
} 


//---------------------------------------------------------------------


exports.getImmagine = (req, res) => { 
  const defaultImage = path.join(__dirname, `../images/Default.jpg`)
  const imagePath = path.join(__dirname, `../images/${req.params.studenteId}.jpg`)
  if(fs.existsSync(imagePath)){  
    res.sendFile(imagePath);
  } else {    
    res.sendFile(defaultImage);
  } 
} 


//---------------------------------------------------------------------


exports.uploadImage = (req, res) => {
   
  var img = req.body.file;
  var data = img.replace(/^data:image\/\w+;base64,/, "");
  var buf = Buffer.from(data, 'base64');
  
  fs.writeFile(path.join(__dirname, `../images/${req.params.studenteId}.jpg`), buf, function(err){
    if(err){
      res.status(500).send({
        message:
          err.message || "Image isn't in .pgn || .jpg or it is to large"   
      })
    } else {
      res.send(true);
    }
  });  
}
//---------------------------------------------------------------------


  exports.getMaxID = (req, res) => {
    Studente.getMaxID((err, data) => {
      res.send(data)
      console.log(data)
    });
  }