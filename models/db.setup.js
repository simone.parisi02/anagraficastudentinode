const mysql = require("mysql");
const dbConfig = require("../config/db.config.js");
const fs = require('fs');
const path = require("path");


// Create a connection to the database
const connection = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

// open the MySQL connection
connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

if(dbConfig.autoRunSql){
  const dataSql = fs.readFileSync(path.join(__dirname, `../SQL/AnagraficaSQL.sql`)).toString();
  const dataArr = dataSql.toString().split(';');

  dataArr.forEach((query) => {
    if(query) {
      // Add the delimiter back to each query before you run them
      // In my case the it was `);`
      query += ';';
      connection.query(query, (err) => {
        if(err) throw err;
      });
    }
  });
}
module.exports = connection;