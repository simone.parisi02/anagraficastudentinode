const { response } = require("express");
const sql = require("./db.setup.js");

//-------------------------------------------------------------------------


// constructor
const Studente = function(studente) {
  this.nome = studente.nome;
  this.cognome = studente.cognome;
  this.anno = studente.anno;
  this.sezione = studente.sezione;
  this.specializzazione = studente.specializzazione;
  this.residenza =studente.residenza;
  this.dataNascita = studente.dataNascita;
  this.Foto = studente.Foto;
};

//---------------------------------------------------------------------


var maxID;
sql.query("SELECT max(ID) as maxID FROM alunni2004", (err, res) => {
  if (err) {
    console.log("error: ", err);
    maxID = 0;
    return 0;
  }
  const response = Object.values(JSON.parse(JSON.stringify(res)))[0].maxID
  maxID = response;
  //console.log(`Max id found ${maxID}`);
  return response;
});


//---------------------------------------------------------------------


Studente.getAll = result => {
  sql.query("SELECT Specializzazione, Anno, Sezione, Cognome, Nome, ID, Residenza, DATE_FORMAT(DataNascita, '%Y-%m-%d') as DataNascita FROM alunni2004", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};


//---------------------------------------------------------------------


Studente.findById = (studenteId, result) => {
  //console.log(`Finding student ${studenteId}`)
  sql.query(`SELECT Specializzazione, Anno, Sezione, Cognome, Nome, ID, Residenza, DATE_FORMAT(DataNascita, '%Y-%m-%d') as DataNascita FROM alunni2004 WHERE ID = ${studenteId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      //console.log("found studente: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Studente with the id
    result({ kind: "not_found" }, null);
  });
};


//---------------------------------------------------------------------


Studente.deleteById = (studenteId, result) => {
  //console.log(`Deleting student ${studenteId}`)
  sql.query(`DELETE FROM alunni2004 WHERE ID = ${studenteId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, false);   
      return;
    }    
    result(null, true);
    return;   
  });
};


//---------------------------------------------------------------------


Studente.getMaxID = (result) => {
  //console.log(maxID);
  result(null,{id:maxID});
}


//---------------------------------------------------------------------


Studente.create = (body, result) => {
  console.log("Creating a student")
  // Data new user
  let Specializzazione = body.Specializzazione;
  let Anno = body.Anno;
  let Sezione = body.Sezione;
  let Nome = body.Nome.toUpperCase();
  let Cognome = body.Cognome.toUpperCase();
  let Residenza = body.Residenza.toUpperCase();
  let DataNascita = body.DataNascita;
  let Foto = body.Foto;
  //console.log(body);
  sql.query(`INSERT INTO alunni2004 values ('${Specializzazione}','${Anno}','${Sezione}','${Cognome}','${Nome}','${++maxID}','${Residenza}','${DataNascita}')`, (err, res) => {
  //sql.query(`INSERT INTO alunni2004 values ('de','212','3d','PArisi','Simone','${++maxID}','Nogaredo','1987-03-29','ciao')`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, false);
      return;
    }
    console.log(`User ${maxID} added succesfully`);

    result(null, true);
    return;
  });
}


//---------------------------------------------------------------------


Studente.deleteAll = (result) => {
  //console.log("Deleting all students")
  sql.query(`DELETE FROM alunni2004`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, false);
      return;
    }    
    result(null, true);
    return;
  })
}


//---------------------------------------------------------------------


Studente.update = (body,id,result) => {
  //console.log(`Updating student ${id}`)
  // Data new user
  let Specializzazione = body.Specializzazione;
  let Anno = body.Anno;
  let Sezione = body.Sezione;
  let Nome = body.Nome.toUpperCase();
  let Cognome = body.Cognome.toUpperCase();
  let Residenza = body.Residenza.toUpperCase();
  let DataNascita = body.DataNascita;
  let Foto = body.Foto;
  //console.log(body);
  //console.log(id);
  sql.query(`UPDATE alunni2004 SET Specializzazione = '${Specializzazione}', Anno = '${Anno}' ,Sezione = '${Sezione}' ,Cognome = '${Cognome}' ,Nome = '${Nome}' ,Residenza = '${Residenza}' ,DataNascita = '${DataNascita}' WHERE id = '${id}';`, (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(err, false);
      return;
    }    
    result(null, true);
    return;
  })
}


//---------------------------------------------------------------------


Studente.getSezioni = result => {
  sql.query("SELECT distinct Sezione FROM alunni2004 order by Sezione", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};


//---------------------------------------------------------------------


Studente.getSpecializzazioni = result => {
  sql.query("SELECT distinct Specializzazione FROM alunni2004 order by Specializzazione", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};


//---------------------------------------------------------------------


Studente.getAnni = result => {
  sql.query("SELECT distinct Anno FROM alunni2004 order by Anno", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

module.exports = Studente;